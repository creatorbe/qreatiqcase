package com.hasan.qreatiqcase.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Response{

	@SerializedName("Type")
	private String type;

	@SerializedName("Description")
	private String description;

	@SerializedName("Message")
	private String message;

	@SerializedName("IdAccount")
	private int idAccount;

	@SerializedName("Token")
	private String token;

	@SerializedName("Code")
	private int code;

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setIdAccount(int idAccount){
		this.idAccount = idAccount;
	}

	public int getIdAccount(){
		return idAccount;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"type = '" + type + '\'' + 
			",description = '" + description + '\'' + 
			",message = '" + message + '\'' + 
			",idAccount = '" + idAccount + '\'' + 
			",token = '" + token + '\'' + 
			",code = '" + code + '\'' + 
			"}";
		}
}