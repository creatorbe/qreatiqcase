package com.hasan.qreatiqcase.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hasan.qreatiqcase.R;
import com.pixplicity.easyprefs.library.Prefs;

public class AccountActivity extends AppCompatActivity {

    EditText mPass;
    Button mOut;
    Context c;
    TextView mEmail,mPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        c = this;
        mPass = findViewById(R.id.input_password);
        mEmail = findViewById(R.id.tvEmail);
        mOut = findViewById(R.id.btnSignOut);
        mPin = findViewById(R.id.tvPin);
        mPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(c, "Sorry sir, the developer need more time to implemention this :)", Toast.LENGTH_SHORT).show();
            }
        });

        mPass.setText(Prefs.getString("pass",null));
        mEmail.setText(Prefs.getString("email",null));
        mOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefs.clear();
                Intent i = new Intent(c,SignActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

    }
}
