package com.hasan.qreatiqcase.activities;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.hasan.qreatiqcase.BE;
import com.hasan.qreatiqcase.R;
import com.pixplicity.easyprefs.library.Prefs;

public class SplashActivity extends Activity {

//    transparency level
//    100% — FF
//    95% — F2
//    90% — E6
//    85% — D9
//    80% — CC
//    75% — BF
//    70% — B3
//    65% — A6
//    60% — 99
//    55% — 8C
//    50% — 80
//    45% — 73
//    40% — 66
//        35% — 59
//        30% — 4D
//        25% — 40
//        20% — 33
//        15% — 26
//        10% — 1A
//         5% — 0D
//        0% — 00

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView mAppVersion = findViewById(R.id.tvVersion);
        mAppVersion.setText("Versi "+ BE.appVersion);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(null != Prefs.getString("email",null)) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            }
        },3000);
    }
}
