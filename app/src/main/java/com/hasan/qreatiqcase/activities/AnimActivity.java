package com.hasan.qreatiqcase.activities;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.hasan.qreatiqcase.R;

public class AnimActivity extends AppCompatActivity {
    private ImageView container;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim);

        container = findViewById(R.id.iv_icons);
        container.setBackgroundResource(R.drawable.anim_splash);

        animationDrawable = (AnimationDrawable) container.getBackground();

    }

    @Override
    protected void onResume() {
        super.onResume();

        animationDrawable.start();

//        checkAnimationStatus(10000, animationDrawable); delay 10s tapi terlalu lama
        checkAnimationStatus(3000, animationDrawable);
    }

    private void checkAnimationStatus(final int time, final AnimationDrawable animationDrawable) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (animationDrawable.getCurrent() != animationDrawable.getFrame(animationDrawable.getNumberOfFrames() - 1))
                    checkAnimationStatus(time, animationDrawable);
                else
                    startActivity(new Intent(getApplicationContext(),SignActivity.class));
            }
        }, time);
    }
}