package com.hasan.qreatiqcase.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.hasan.qreatiqcase.BE;
import com.hasan.qreatiqcase.R;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    View mReg,mTami;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = this;
        Date date = new Date();
        Date myDate = new Date(Prefs.getLong("time_expired", 0));

        if(date.getTime() < myDate.getTime()){
            Toast.makeText(this, "Welcome "+Prefs.getString("email",null), Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Sorry, your session is expired try to re login", Toast.LENGTH_LONG).show();
            Intent i = new Intent(c,SignActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

        Date after2Hours = new Date (date.getTime () + 2L * 60L * 60L * 1000L);
        Prefs.putLong("time_expired",after2Hours.getTime());

        mReg = findViewById(R.id.llRegina);
        mReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "You don't have access to view this", Toast.LENGTH_SHORT).show();
            }
        });

        mTami = findViewById(R.id.llTami);
        mTami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(c,AccountActivity.class));
            }
        });
    }
}
